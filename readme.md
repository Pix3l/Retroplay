# Retroplay

Retroplay is a collection of old Atari style games, like **Pong**, **Breakout** and **Tetris**.

## Requirements

All of these games requires **SDL 1.2** and it's **TTF** and **Mixer** extension to be compiled and executed.
You should found those libraries in your favorite distro repository or you can find it at [https://www.libsdl.org/](https://www.libsdl.org/)

## Compile

You should find the compilation command at top of every sources, like these:

```
gcc -o retropong retropong.c -lSDL -lSDL_ttf -lSDL_mixer 
```

## Instruction

Use **arrow keys** to switch between menu options or to move player 1, switch menu parameter with **left** and **right** arrow keys.
Use **Enter** key to make a choice or start a new game.

Some games can be played by 2 players, in 2 player mode, **Q** and **A** keys moves **player 1**, **arrow** keys move **player 2**. 

## Assets

Sounds effects are from [https://freesound.org](https://freesound.org) and released under under the [Creative Commons Attribution License](https://creativecommons.org/licenses/by/3.0/).

Font **retrofont.ttf** is from [http://fontstruct.fontshop.com] and released under under the [Attribution-NonCommercial-NoDerivs 3.0 Unported (CC BY-NC-ND 3.0)](https://creativecommons.org/licenses/by-nc-nd/3.0/).

## License

This game is released as open source under [GPL 2.0 license](https://www.gnu.org/licenses/gpl-2.0.html), You should have received a copy of the **GNU General Public License along with this program;** if not, write to the **Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.**

This game is provided without warranty, use it at your own risk!

## Author

This game was written in 2010 by **Cerullo Davide** [pix3lworkshop.altervista.org](pix3lworkshop.altervista.org). 
