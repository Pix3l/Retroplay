/**********************************************************************
 *                                                                    *
 *                             RetroBreak                             *
 *                                                                    *
 *            A simple SDL based breakout game with sound!            *
 *      gcc -o retrobreak retrobreak.c -lSDL -lSDL_ttf -lSDL_mixer    *
 *                                                                    *
 *    Instructions: Arrows move the paddle, destroy how many bricks   *
 *    as you can to make an highscore. Don't miss your ball or you    *
 *    will lose a life, 3 life per play.                              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *                                                                    *
 *   Release 2 - Fixed bricks collision bug                           *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <time.h>

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

int PADDLE_WIDTH = 60; //Need as variable to be settable
#define PADDLE_HEIGHT 10 

#define BALL_WIDTH 10
#define BALL_HEIGHT 10

#define BRICK_WIDTH 20
#define BRICK_HEIGHT 10
#define BRICKS 96 //Number of total bricks per level

#define COLS 16
#define ROWS 12

#define FONT "retrofont.ttf"
#define FONT_SIZE 36

#define TIME_STOP 2000

    //Global variables
    int t, tl = 0, frequency = 1000 / 100, temp, t2; //Timers variables
    int repeat, i, j, k=0, indexB=0, quit=0, paused=0, shoot=0, choice=0; //Various in-game variables
    SDL_Event event;
    SDL_Surface *screen, *textSurface;
    SDL_Rect rect; //Main drawing surface
    Uint8 *keystate; // keyboard state
    char text[20]; //Hold text
    //Audio variables
	Mix_Chunk *sound = NULL;		//Pointer to our sound, in memory
	int channel;				//Channel on which our sound is played
	int audio_rate = 22050;			//Frequency of audio playback
	Uint16 audio_format = AUDIO_S16SYS; 	//Format of the audio we're playing
	int audio_channels = 2;			//2 channels = stereo
	int audio_buffers = 4096;		//Size of the audio buffers in memory
	int bricks=BRICKS;

struct _Player
{
	int x, y;
} Player1, Player2;

struct _Ball
{
	int x, y;
	int slopeX, slopeY, speedX, speedY, size;
} Ball;

struct _Brick
{
	int x, y, w, h, color, hit;
} Brick[96];

struct _Game
{
	int player_score, lives, status, players, difficult;
	TTF_Font *font;
} Game;

void restartTimer()
{
	//Reset frame per screen counter
	t = tl = temp = SDL_GetTicks();
}

void PlaySound(char *file)
{
	//Load our WAV file from disk
	sound = Mix_LoadWAV(file);
	if(sound == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
	}
	
	//Play our sound file, and capture the channel on which it is played
	channel = Mix_PlayChannel(-1, sound, 0);
	if(channel == -1) {
		printf("Unable to play WAV file: %s\n", Mix_GetError());
	}
}

void StopBall()
{
	if(t2<=0)
	 t2 = t;  //last tick since we call this function
	 
	if(t - t2 >= TIME_STOP) //wait SDL_Getticks rich last_tick value
	 {
	  shoot=0;
	  t2 = 0; //reset time variable
	 }
}

void Reset()
{	
	//It's not time to shot
	shoot=1;
	
	//Ball start position
    Ball.x = SCREEN_WIDTH/2;
    Ball.y = 80;
	
    //Player1 start position
    Player1.x = SCREEN_WIDTH/2 - PADDLE_WIDTH/2;
    Player1.y = SCREEN_HEIGHT - 20;
    
    //Ball default slope
    Ball.slopeY = Ball.slopeX = -1;
    
    srand(time(NULL));
    
    Ball.speedX = 1;
    Ball.speedY = 1;
    
    // Flip a coin for x and y directions
    FlipCoin();
}

void Pause()
 {
 	SDL_Delay(2000);
 	restartTimer(); 
 	Reset();
 }

int FlipCoin()
 {
    // Flip a coin for x and y directions
    if ((int)(2.0 * rand() / (RAND_MAX + 1.0)))
     return Ball.slopeX *= -1;
    if ((int)(2.0 * rand() / (RAND_MAX + 1.0)))
     return Ball.slopeY *= -1;
 }

// Check overlapping of 4 points of 2 rects
int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2)
 {
    if (y1+h1 < y2) return 0;
    if (y1 > y2+h2) return 0;
    if (x1+w1 < x2) return 0;
    if (x1 > x2+w2) return 0;
    
    return 1;
 }

void MoveBall()
{   
  //Hit a wall
  if (Ball.x + BALL_WIDTH >= SCREEN_WIDTH | Ball.x < 0)
   { Ball.slopeX = -Ball.slopeX; Ball.x+=Ball.slopeX; PlaySound("pong1.wav"); }

  if(Ball.y < 0)
   { Ball.slopeY = -Ball.slopeY; Ball.y+=Ball.slopeY; PlaySound("pong1.wav"); }
 
  //Hit bottom
  if(Ball.y > SCREEN_HEIGHT - BALL_HEIGHT)
   { PlaySound("pong2.wav"); Pause(); shoot=1; Game.lives--; }

  //Bounce against the paddles
  if ((Ball.y + BALL_HEIGHT == Player1.y) && (Ball.x >= Player1.x && Ball.x <= Player1.x + PADDLE_WIDTH))
   {
   	Ball.slopeY *= -1; PlaySound("pong0.wav");
   }
  
  //are we hit a brick?
  for(i=0; i<BRICKS; i++)
  {
  	if(Brick[i].hit ==0)
  	{
  		if (rectCollision(Ball.x+Ball.slopeX, Ball.y, BALL_WIDTH, BALL_HEIGHT, 
                      Brick[i].x, Brick[i].y, Brick[i].w, Brick[i].h))
         {
         	Brick[i].hit = 1;
         	Ball.slopeX *= -1;
         	bricks--;
         	Game.player_score += 10 - Brick[i].color;
         	PlaySound("pong0.wav");
         }
        
        if (rectCollision(Ball.x, Ball.y+Ball.slopeY, BALL_WIDTH, BALL_HEIGHT, 
                      Brick[i].x, Brick[i].y, Brick[i].w, Brick[i].h))
         {
         	Brick[i].hit = 1;
         	Ball.slopeY *= -1;
         	bricks--;
         	Game.player_score += 10 - Brick[i].color;
         	PlaySound("pong0.wav");
         }
    }
  }
 
 //This make the ball wait 2 seconds before move
 if(!shoot)
  {
   Ball.x += Ball.speedX * Ball.slopeX ;
   Ball.y += Ball.speedY * Ball.slopeY ;
  }
 else
  {
    StopBall();
  }
}

void DrawRect(int x, int y, int width, int height, int color)
 {
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
	    SDL_FillRect(screen, &rect, color);
 }

void DrawText(int x, int y, int width, int height, char *string, void *ptr, Uint8 r, Uint8 g, Uint8 b)
 {
        sprintf(text, string, ptr);
        SDL_Color color = { r, g, b };
        textSurface = TTF_RenderText_Solid(Game.font, text, color);
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
        SDL_BlitSurface(textSurface, NULL, screen, &rect);
 }

int DrawMenu()
 {
  DrawText(10, 20, 0, 0, "RetroBreak", 0,  0xFF, 0xFF, 0xFF);
  DrawRect(10, 50, SCREEN_WIDTH, 10, 0xFF9D2E);
  DrawText(30, 70, 0, 0, "Start", 0, 0xFF, 0xFF, 0xFF);
  DrawText(30, 100, 0, 0, "Difficult %d", (int *) Game.difficult, 0xFF, 0xFF, 0xFF);
  DrawText(30, 130, 0, 0, "Quit", 0, 0xFF, 0xFF, 0xFF);
  DrawRect(Ball.x, Ball.y, BALL_WIDTH, BALL_HEIGHT, 0xFF9D2E);
 }

void DrawPlayfield()
 {
    
    //Scores and lives
    DrawText(10, SCREEN_HEIGHT - 45, 0, 0, "%d", (int *) Game.player_score, 0xFF, 0xAA, 0x00);
 	DrawText(SCREEN_WIDTH - 30, SCREEN_HEIGHT - 45, 0, 0, "%d", (int *) Game.lives, 0xFF, 0xFF, 0x00);
 	
 for (i = 0; i <BRICKS; i++)
     {
     	if(Brick[i].hit ==0)
     	{
     		if(Brick[i].color==0)
     		 DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0x8B15AA);
     		if(Brick[i].color==1)
	         DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0xE73595);
	        if(Brick[i].color==2)
	         DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0xC91495);
	        if(Brick[i].color==3)
	         DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0xFF9D2E);
	        if(Brick[i].color==4)
	         DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0xB5DD56);
	        if(Brick[i].color==5)
	         DrawRect(Brick[i].x, Brick[i].y, BRICK_WIDTH, BRICK_HEIGHT, 0x7F6DFF);
		 }
    }
  	
    //Players
    DrawRect(Player1.x, Player1.y, PADDLE_WIDTH, PADDLE_HEIGHT, 0x8B15AA);
    
    //Ball
    DrawRect(Ball.x, Ball.y, BALL_WIDTH, BALL_HEIGHT, 0x8B15AA);

   //End game when all bricks are destroyed
   if(bricks==0)
    {
     t2=0; //Reset StopBall timer
	 SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0)); //Clear the screen
	 DrawText(10, SCREEN_HEIGHT/2, 0, 0, "Player %d wins!", (int *) 1,  0xFF, 0x00, 0x00);
	 SDL_Flip(screen); //Update the screen
	 SDL_Delay(3000);
	 Game.status = 0;
	}

 }

void Draw()
{
  if(Game.status)
   {
   	DrawPlayfield();
   }
  else
   {
   	DrawMenu();
   }
   
   //Terminating the play when a score is ritched
   if(Game.lives==0 && Game.status==1)
    {
     t2=0; //Reset StopBall timer
	 SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0)); //Clear the screen
	 DrawText(10, SCREEN_HEIGHT/2, 0, 0, "Your score is", NULL,  0xFF, 0x00, 0x00);
	 DrawText(SCREEN_WIDTH/2, SCREEN_HEIGHT/2+40, 0, 0, "%d", (void *) (Game.player_score),  0xFF, 0x00, 0x00);
	 SDL_Flip(screen); //Update the screen
	 Pause();
	 Game.status = 0;
	}
}

int getInput()
{
 // Grab a keystate snapshot
 keystate = SDL_GetKeyState( NULL );
 /* Handle key presses */
 if(Ball.y < Player1.y)
  {
   if (keystate[SDLK_LEFT] && (Player1.x!=0))
    Player1.x--;
   if (keystate[SDLK_RIGHT] && (Player1.x + PADDLE_WIDTH <= SCREEN_WIDTH))
    Player1.x++;
  }
}

void newGame()
 {
	/* Init ingame variables */
	Game.players=1;
	Game.difficult=1;
	Game.lives = 3;
    Game.status = 1;
    bricks = COLS * 6; // number of bricks

	for (i = 0; i <6; i++)
	{
		for (j = 0; j <16; j++)
		{
			Brick[indexB].x=j*BRICK_WIDTH;
			Brick[indexB].y=i*BRICK_HEIGHT;
			Brick[indexB].w=BRICK_WIDTH;
			Brick[indexB].h=BRICK_HEIGHT;
			Brick[indexB].color=k;		
			indexB++;
		}
     k++;
    }
	Reset();
 }

void gameMenu()
 {
 	static int press, choice;
 	
    switch(choice)
	 {
	 	case -1: //Scroll back
	 	 choice=3;
	 	 break;
	 	case 0: //Start game
	 	 Ball.x = 10; Ball.y = 77;
	 	 if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN)
	 	  {
	 	  	newGame();
	      }
	 	 break;
	 	case 1: //Difficult
	 	 Ball.x = 10; Ball.y = 107;
	 	 if (event.key.keysym.sym == SDLK_LEFT)
	 	  {
	       Game.difficult=1;
	       PADDLE_WIDTH = 60;
	      }
	 	 if (event.key.keysym.sym == SDLK_RIGHT)
	 	  {
	       Game.difficult=2;
	       PADDLE_WIDTH = 30;
	      }
	 	 break;
	 	case 2: //exit
	 	 Ball.x = 10; Ball.y = 137;
	 	 if (event.key.keysym.sym == SDLK_RETURN)
	 	  quit = 1;
	     break;
	 	default:
	 	 choice=0;
	 }
	 
   if(event.type == SDL_KEYDOWN && !press) //On keypress we switch the menu
 	{
 	 if (event.key.keysym.sym == SDLK_UP)
 	  {
 	   choice--;
 	   press=1;
 	  }
 	 if (event.key.keysym.sym == SDLK_DOWN)
 	  {
 	   choice++;
 	   press=1;
 	  }
 	}
   if(event.type == SDL_KEYUP) //If key is released let the player switch the menu
    press=0;
 }

int fps_sync ()
{
	t = SDL_GetTicks ();
	if (t - tl >= frequency)
	{
		temp = (t - tl) / frequency; //delta time
		tl += temp * frequency;
		return temp;
	}
	else
	{
		SDL_Delay (frequency - (t - tl));
		tl += frequency;
		return 1;
	}
}

void logicLoop()
 {
  if(!paused)
   {
	getInput();
	MoveBall();
   }
 }

int mainLoop()
 {
  //main game loop
  while(!quit)
   {
   	SDL_PollEvent(&event);
	repeat = fps_sync ();
		for (i = 0; i < repeat; i ++)
		 {
	       	if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
	       	 quit = 1; /* Window closed */

   	      switch(Game.status)
   	       {
   	       	case 0: //Menu
   	       	 gameMenu();
   	       	 break;
   	       	case 1: //Play
   	       	 logicLoop();
   	       	 break;
   	       }
		 }
		//clear the screen
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
		//draw to the screen
		Draw();
		//update the screen
		SDL_Flip(screen);
   }
 }

int main(int argc, char *argv[])
{
  // init video stuff
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

   // init screen
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE);
	if(screen == NULL)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}

    // init ttf and load font
    TTF_Init();
    if ((Game.font = TTF_OpenFont( FONT, FONT_SIZE )) == NULL)
    {
     fprintf( stderr, "Could not open font: %s\n", FONT );
     exit(-1);
    }

	//Initialize SDL_mixer with our chosen audio settings
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}

	SDL_WM_SetCaption("RetroBreak", "RetroBreak");
	
	newGame();
	
	Game.status=0; //Game status 0 means Game Menu
	
	//main game loop
	mainLoop();
	printf("Thanks for playing!\n");
   	SDL_Quit();
	return 0;
}
