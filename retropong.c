/**********************************************************************
 *                                                                    *
 *                            RetroPong                               *
 *                                                                    *
 *               A simple SDL based pong game with sound!             *
 *       gcc -o retropong retropong.c -lSDL -lSDL_ttf -lSDL_mixer     *
 *                                                                    *
 *    Instructions: Arrows move player 1, key Q and A move player 2   *
 *    In 2 player game, Q and A move player 1, arrows player 2        *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <time.h>

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

#define PADDLE_WIDTH 10
int PADDLE_HEIGHT=60; //Need as variable to be settable

#define BALL_WIDTH 10
#define BALL_HEIGHT 10

#define FONT "retrofont.ttf"
#define FONT_SIZE 36

#define TIME_STOP 2000

    //Global variables
    int t, tl = 0, frequency = 1000 / 100, temp, t2; //Timers variables
    int repeat, i, quit=0, paused=0, shoot=0, choice=0; //Various in-game variables
    SDL_Event event;
    SDL_Surface *screen, *textSurface;
    SDL_Rect rect; //Main drawing surface
    Uint8 *keystate; // keyboard state
    char text[20]; //Hold text
    //Audio variables
	Mix_Chunk *sound = NULL;		//Pointer to our sound, in memory
	int channel;				//Channel on which our sound is played
	int audio_rate = 22050;			//Frequency of audio playback
	Uint16 audio_format = AUDIO_S16SYS; 	//Format of the audio we're playing
	int audio_channels = 2;			//2 channels = stereo
	int audio_buffers = 4096;		//Size of the audio buffers in memory

struct _Player
{
	int x, y;
} Player1, Player2;

struct _Ball
{
	int x, y;
	int slopeX, slopeY, speedX, speedY, size;
} Ball;

struct _Game
{
	int player_score, cpu_score, status, players, difficult;
	TTF_Font *font;
} Game;

void restartTimer()
{
	//Reset frame per screen counter
	t = tl = temp = SDL_GetTicks();
}

void PlaySound(char *file)
{
	//Load our WAV file from disk
	sound = Mix_LoadWAV(file);
	if(sound == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
	}
	
	//Play our sound file, and capture the channel on which it is played
	channel = Mix_PlayChannel(-1, sound, 0);
	if(channel == -1) {
		printf("Unable to play WAV file: %s\n", Mix_GetError());
	}
}

void StopBall()
{
	if(t2<=0)
	 t2 = t;  //last tick since we call this function
	 
	if(t - t2 >= TIME_STOP) //wait SDL_Getticks rich last_tick value
	 {
	  shoot=0;
	  t2 = 0; //reset time variable
	 }
}

void Reset()
{	
	//It's not time to shot
	shoot=1;
	
	//Ball start position
    Ball.x = SCREEN_WIDTH/2;
    Ball.y = SCREEN_HEIGHT/2;
	
    //Player1 start position
    Player1.x = 20;
    Player1.y = SCREEN_HEIGHT/2 - PADDLE_HEIGHT/2;
    
    //Player2 start position
    Player2.x = SCREEN_WIDTH - 30;
    Player2.y = SCREEN_HEIGHT/2 - PADDLE_HEIGHT/2;
    
    //Ball default slope
    Ball.slopeY = Ball.slopeX = -1;
    
    srand(time(NULL));
    
    Ball.speedX = 2;
    Ball.speedY = 1;
    
    // Flip a coin for x and y directions
    FlipCoin();
}

void Pause()
 {
 	SDL_Delay(2000);
 	restartTimer(); 
 	Reset();
 }

int FlipCoin()
 {
    // Flip a coin for x and y directions
    if ((int)(2.0 * rand() / (RAND_MAX + 1.0)))
     return Ball.slopeX *= -1;
    if ((int)(2.0 * rand() / (RAND_MAX + 1.0)))
     return Ball.slopeY *= -1;
 }

void MoveCpu()
{
  if(Ball.slopeX > 0 && shoot==0)
   {
    if(Player2.y < Ball.y && Player2.y + PADDLE_HEIGHT < SCREEN_HEIGHT)
     Player2.y += 1;
    if(Player2.y > Ball.y)
     Player2.y -= 1;
   }
}

void MoveBall()
{
  //P1 has scored
  if (Ball.x >= (SCREEN_WIDTH - BALL_WIDTH))
   {
    Game.player_score++;
    PlaySound("pong2.wav");
    Pause();
    shoot=1;
   }

  //P2 has scored
  if (Ball.x < 0)
   {
   	Game.cpu_score++;
   	PlaySound("pong2.wav");
   	Pause();
   	shoot=1;
   }

  //Bounce top or down
  if(Ball.y < 0 || Ball.y > SCREEN_HEIGHT - BALL_HEIGHT)
   { Ball.slopeY = -Ball.slopeY; Ball.y+=Ball.slopeY; PlaySound("pong1.wav"); 
   }

  //Bounce against the paddles
  if (((Ball.x == Player1.x + PADDLE_WIDTH) && (Ball.y >= Player1.y && ((Ball.y + BALL_HEIGHT) <= Player1.y + PADDLE_HEIGHT))))
   {
   	Ball.slopeX *= -1; Ball.slopeY = rand() % 5 -2; PlaySound("pong0.wav");
   }

  if ((Ball.x == Player2.x - PADDLE_WIDTH) && (Ball.y >= Player2.y && ((Ball.y + BALL_HEIGHT) <= Player2.y + PADDLE_HEIGHT)))
   {
   	Ball.slopeX *= -1; Ball.slopeY = rand() % 5 -2; PlaySound("pong0.wav");
   }

 //This make the ball wait 2 seconds before move
 if(!shoot)
  {
   Ball.x += Ball.speedX * Ball.slopeX ;
   Ball.y += Ball.speedY * Ball.slopeY ;
  }
 else
  {
    StopBall();
  }
}

void DrawRect(int x, int y, int width, int height, int color)
 {
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
	    SDL_FillRect(screen, &rect, color);
 }

void DrawText(int x, int y, int width, int height, char *string, void *ptr, Uint8 r, Uint8 g, Uint8 b)
 {
        sprintf(text, string, ptr);
        SDL_Color color = { r, g, b };
        textSurface = TTF_RenderText_Solid(Game.font, text, color);
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
        SDL_BlitSurface(textSurface, NULL, screen, &rect);
 }

int DrawMenu()
 {
  DrawText(10, 20, 0, 0, "RetroPong", 0,  0xFF, 0xFF, 0xFF);
  DrawRect(10, 50, SCREEN_WIDTH, 10, 0xff0000);
  DrawText(30, 70, 0, 0, "Start", 0, 0xFF, 0xFF, 0xFF);
  DrawText(30, 100, 0, 0, "Players %d", (int *) Game.players, 0xFF, 0xFF, 0xFF);
  DrawText(30, 130, 0, 0, "Difficult %d", (int *) Game.difficult, 0xFF, 0xFF, 0xFF);
  DrawText(30, 160, 0, 0, "Quit", 0, 0xFF, 0xFF, 0xFF);
  DrawRect(Ball.x, Ball.y, BALL_WIDTH, BALL_HEIGHT, 0xff0000);
 }

void DrawPlayfield()
 {
   	//Playfield
    DrawRect(0, 5, SCREEN_WIDTH, 10, 0xffffff);
    DrawRect(0, SCREEN_HEIGHT - 15, SCREEN_WIDTH, 10, 0xffffff);
    DrawRect(SCREEN_WIDTH / 2, 5, 10, SCREEN_HEIGHT - 10, 0xffffff);
    
    //Scores
    DrawText(SCREEN_WIDTH / 2 - 40, 20, 0, 0, "%d", (int *) Game.player_score, 0xFF, 0xFF, 0xFF);
 	DrawText(SCREEN_WIDTH / 2 + 40, 20, 0, 0, "%d", (int *) Game.cpu_score, 0xFF, 0xFF, 0xFF);
    
    //Players
    DrawRect(Player1.x, Player1.y, PADDLE_WIDTH, PADDLE_HEIGHT, 0xC046C0);
    DrawRect(Player2.x, Player2.y, PADDLE_WIDTH, PADDLE_HEIGHT, 0xC046C0);
    
    //Ball
    DrawRect(Ball.x, Ball.y, BALL_WIDTH, BALL_HEIGHT, 0xff0000);
 }

void Draw()
{
  if(Game.status)
   {
   	DrawPlayfield();
   }
  else
   {
   	DrawMenu();
   }
   
   //Terminating the play when a score is ritched
   if(Game.player_score==9 | Game.cpu_score==9 && Game.status==1)
    {
     t2=0; //Reset StopBall timer
	 SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0)); //Clear the screen
	 DrawText(10, SCREEN_HEIGHT/2, 0, 0, "Player %d wins!", (void *) ((Ball.x > SCREEN_WIDTH/2) ? 1 : 2),  0xFF, 0x00, 0x00);
	 SDL_Flip(screen); //Update the screen
	 SDL_Delay(3000);
	 Game.status = 0;
	}
}

int getInput()
{
 // Grab a keystate snapshot
 keystate = SDL_GetKeyState( NULL );
 /* Handle key presses */
 if (keystate[(Game.players==2 ? SDLK_q : SDLK_UP)] && (Player1.y!=0))
  Player1.y--;
 if (keystate[(Game.players==2 ? SDLK_a : SDLK_DOWN)] && (Player1.y + PADDLE_HEIGHT != SCREEN_HEIGHT))
  Player1.y++;
}

void player2()
 {
  // Assing new keys to player 1
  if (keystate[SDLK_UP] && (Player2.y!=0))
   Player2.y--;
  if (keystate[SDLK_DOWN] && (Player2.y + PADDLE_HEIGHT != SCREEN_HEIGHT))
   Player2.y++;
 }

void gameMenu()
 {
 	static int press, choice;

    switch(choice)
	 {
	 	case -1: //Scroll back
	 	 choice=3;
	 	 break;
	 	case 0: //Start game
	 	 Ball.x = 10; Ball.y = 77;
	 	 if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_RETURN)
	 	  {
	       Game.status = 1;
	       Game.player_score = Game.cpu_score = 0; //Reset scores
	       Reset();
	      }
	 	 break;
	 	case 1: //Players
	 	 Ball.x = 10; Ball.y = 107;
	 	 if (event.key.keysym.sym == SDLK_RIGHT)
	 	  {
	       Game.players=2;
	      }
	 	 if (event.key.keysym.sym == SDLK_LEFT)
	 	  {
	       Game.players=1;
	      }
	 	 break;
	 	case 2: //Difficult
	 	 Ball.x = 10; Ball.y = 137;
	 	 if (event.key.keysym.sym == SDLK_LEFT)
	 	  {
	       Game.difficult=1;
	       PADDLE_HEIGHT = 60;
	      }
	 	 if (event.key.keysym.sym == SDLK_RIGHT)
	 	  {
	       Game.difficult=2;
	       PADDLE_HEIGHT = 30;
	      }
	 	 break;
	 	case 3: //exit
	 	 Ball.x = 10; Ball.y = 167;
	 	 if (event.key.keysym.sym == SDLK_RETURN)
	 	  quit = 1;
	     break;
	 	default:
	 	 choice=0;
	 }
	 
   if(event.type == SDL_KEYDOWN && !press) //On keypress we switch the menu
 	{
 	 if (event.key.keysym.sym == SDLK_UP)
 	  {
 	   choice--;
 	   press=1;
 	  }
 	 if (event.key.keysym.sym == SDLK_DOWN)
 	  {
 	   choice++;
 	   press=1;
 	  }
 	}
   if(event.type == SDL_KEYUP) //If key is released let the player switch the menu
    press=0;
 }

int fps_sync ()
{
	t = SDL_GetTicks ();
	if (t - tl >= frequency)
	{
		temp = (t - tl) / frequency; //delta time
		tl += temp * frequency;
		return temp;
	}
	else
	{
		SDL_Delay (frequency - (t - tl));
		tl += frequency;
		return 1;
	}
}

void logicLoop()
 {
  if(!paused)
   {
	getInput();
	if(Game.players==1)
	 MoveCpu();
	else
	 player2();
	MoveBall();
   }
 }

int mainLoop()
 {
  //main game loop
  while(!quit)
   {
   	SDL_PollEvent(&event);
	repeat = fps_sync ();
		for (i = 0; i < repeat; i ++)
		 {
	       	if (event.type == SDL_QUIT || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
	       	 quit = 1; /* Window closed */

   	      switch(Game.status)
   	       {
   	       	case 0: //Menu
   	       	 gameMenu();
   	       	 break;
   	       	case 1: //Play
   	       	 logicLoop();
   	       	 break;
   	       }
		 }
		//clear the screen
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
		//draw to the screen
		Draw();
		//update the screen
		SDL_Flip(screen);
   }
  //check what quit the game
 }

int main(int argc, char *argv[])
{
  // init video stuff
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

   // init screen
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE);
	if(screen == NULL)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}

    // init ttf and load font
    TTF_Init();
    if ((Game.font = TTF_OpenFont( FONT, FONT_SIZE )) == NULL)
    {
     fprintf( stderr, "Could not open font: %s\n", FONT );
     exit(-1);
    }

	//Initialize SDL_mixer with our chosen audio settings
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}

	SDL_WM_SetCaption("RetroPong", "RetroPong");
	
	/* Init ingame variables */
	Game.status=0; //Game status 0 means Game Menu
	Game.players=1;
	Game.difficult=1;
	
	//main game loop
	mainLoop();
	printf("Thanks for playing!\n");
   	SDL_Quit();
	return 0;
}
