/**********************************************************************
 *                                                                    *
 *                            RetroTetris                             *
 *                                                                    *
 *             A simple SDL based tetris game with sound!             *
 *     gcc -o retrotetris retrotetris.c -lSDL -lSDL_ttf -lSDL_mixer   *
 *                                                                    *
 *    Instructions: Left, Right and Down arrows move the tetramini    *
 *    in the game board. Make a coplete line for make the disappear.  *
 *         If tetramini in the board reach the top you lose!          *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *      Some part of this game are derived and reprogrammed from      *
 * Javier López (javilop.com) and Evil_Greven (gamedev.net) tutorials * 
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <time.h>

#define SCREEN_WIDTH 330
#define SCREEN_HEIGHT 320

#define TILESIZE 16
#define PIECE_SIZE 4

#define MAP_W 12 //Aggiungere +2 per le colonne
#define MAP_H 20

#define FONT "retrofont.ttf"
#define FONT_SIZE 36

#define TIME_STOP 2000

#define GREY 0xC0C0C0
#define GREEN 0x008000
#define BLUE 0x0000FF
#define RED 0xFF0000
#define ORANGE 0xFF9D2E
#define WHITE 0xFFFFFF
#define PURPLE 0xFF00FF
#define YELLOW 0xFFFF00
#define SKYBLUE 0x8080FF

 /*enum _Colors { GREY = 0xC0C0C0, GREEN = 0x008000, BLUE = 0x0000FF, 
               RED = 0xFF0000, ORANGE = 0xFF9D2E, WHITE = 0xFFFFFF, 
               PURPLE = 0xFF00FF, YELLOW = 0xFFFF00, SKYBLUE = 0x8080FF
             }*/

    //Global variables
    int t, tl = 0, frequency = 1000 / 100, temp, t2; //Timers variables
    int repeat, i, quit=0, paused=0, shoot=0, choice=0; //Various in-game variables
    SDL_Event event;
    SDL_Surface *screen, *textSurface;
    SDL_Rect rect; //Main drawing surface
    SDLKey keyPressed; // keyboard state
    char text[20]; //Hold text
    //Audio variables
	Mix_Chunk *sound = NULL;		//Pointer to our sound, in memory
	int channel;				//Channel on which our sound is played
	int audio_rate = 22050;			//Frequency of audio playback
	Uint16 audio_format = AUDIO_S16SYS; 	//Format of the audio we're playing
	int audio_channels = 2;			//2 channels = stereo
	int audio_buffers = 4096;		//Size of the audio buffers in memory
	
	int bricks;

int playground[MAP_H][MAP_W];

struct _Ball
{
	int x, y;
} Ball;

struct _Game
{
	int score, level, lines, status;
	TTF_Font *font;
} Game;

struct Piece {
	int size[PIECE_SIZE][PIECE_SIZE]; //Actual tetramino
	int next[PIECE_SIZE][PIECE_SIZE]; //Next tetramino
	int x, y;
} sPiece;

void PlaySound(char *file)
{
	//Load our WAV file from disk
	sound = Mix_LoadWAV(file);
	if(sound == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
	}
	
	//Play our sound file, and capture the channel on which it is played
	channel = Mix_PlayChannel(-1, sound, 0);
	if(channel == -1) {
		printf("Unable to play WAV file: %s\n", Mix_GetError());
	}
}

void createTetramini()
 {
	int tetramino;
	int i,j;
	//  0   1   2   3   4    5   6    
	//   X                             These
	//   X   XX   X  XX   XX  XX   XX  are
	//   X   XX  XXX  XX XX    X   X   block
	//   X                     X   X   types

	sPiece.x=MAP_W/2-2;
	sPiece.y=0;
	
	/** Actual tetramino **/
	
	//Random choice between 7 blocks
	tetramino=rand()%7;

	//Reset actual and next tetramino
	for(i=0; i<4; i++)
	 for(j=0; j<4; j++)
	  {
	   sPiece.next[i][j]=0;
	  }

	switch (tetramino)
	 {
	  case 0: //Bar
	   {
		sPiece.next[1][0]=1;
		sPiece.next[1][1]=1;
		sPiece.next[1][2]=1;
		sPiece.next[1][3]=1;
		sPiece.y=0;
		break;
	   }
      case 1: //Box
	   {
		sPiece.next[1][1]=2;
		sPiece.next[1][2]=2;
		sPiece.next[2][1]=2;
		sPiece.next[2][2]=2;
	    break;
	   }
	  case 2: //T
	   {
		sPiece.next[1][1]=3;
		sPiece.next[0][2]=3;
		sPiece.next[1][2]=3;
		sPiece.next[2][2]=3;
		break;
	   }
	  case 3://Left Leaner
	   {
		sPiece.next[0][1]=4;
		sPiece.next[1][1]=4;
		sPiece.next[1][2]=4;
		sPiece.next[2][2]=4;
		break;
	   }
	  case 4://Right Leaner
	   {
		sPiece.next[2][1]=5;
		sPiece.next[1][1]=5;
		sPiece.next[1][2]=5;
		sPiece.next[0][2]=5;
		break;
	   }
	  case 5://L inverted
	   {
		sPiece.next[1][1]=6;
		sPiece.next[2][1]=6;
		sPiece.next[2][2]=6;
		sPiece.next[2][3]=6;
		break;
	   }
	  case 6://L regular
	   {
		sPiece.next[2][1]=7;
		sPiece.next[1][1]=7;
		sPiece.next[1][2]=7;
		sPiece.next[1][3]=7;
	    break;
	   }
	 }
 }

void assignTetramini()
 {
 	int i, j;
	
	for(i=0; i<4; i++)
	 for(j=0; j<4; j++)
	  sPiece.size[i][j]=0;
    
    for(i=0; i<4; i++)
	 for(j=0; j<4; j++)
	  sPiece.size[i][j] = sPiece.next[i][j]; //Create actual tetramino
	
	createTetramini();
 }

void oneLineDelete(int row)
{
	int x,y;
	int counter=0;

	for(x=row; x>0; x--)
	 {
	  for(y=0; y<MAP_W; y++)
	   playground[x][y]=playground[x-1][y];
	   
	  if(playground[x][y]!=0)
	   counter++;
	 }

    //Update game status
	Game.lines++;
    if(!(Game.lines % 10)) //Level up every 10 lines
     Game.level++;
    Game.score=Game.score+(5*counter);
}

void multiLineDelete()
{
	int x, y, maxlines;
	
	for(x = sPiece.y; x < MAP_H-1; x++)
	 {
	  y = 1;
	  while (y < MAP_W)
	   { 
	    if(playground[x][y] == 0) break;
	    y++;
	   }
	  if (y == MAP_W)
	   {
	    oneLineDelete(x);
	    maxlines++;
	   }
	 }
}

void RotateBlock()
{
	int i, j, x, y, temp[4][4];

	//copy & rotate the piece to the temporary array
	for(i=0; i<4; i++)
	 for(j=0; j<4; j++)
	   temp[3-j][i]=sPiece.size[i][j];

	//check collision of the temporary array with map borders
	int px, py; /*piece*/
	int mX, mY; /*map*/
	
	 for(mX = sPiece.x, px = 0; mX < (sPiece.x + PIECE_SIZE); mX++, px++)
	   for(mY = sPiece.y, py = 0; mY < (sPiece.y + PIECE_SIZE); mY++, py++)
	    if(temp[py][px] != 0)
	     {
		  /*The piece is outside the limits of the board*/
		  if((mX < 0) || (mX >= MAP_W) || (mY >= MAP_H))
		   return; //exit from function
				
		  /*The temporary piece have collisioned with a block already stored in the map*/
		  if(playground[mY][mX] != 0)
		   return;
		 }
	//end collision check

	//successful! Copy the rotated temporary array to the original piece
	for(i=0; i<4; i++)
		for(j=0; j<4; j++)
			sPiece.size[ i ][j]=temp[ i ][j];
	
	PlaySound("pong0.wav");

	return;
}

int checkMovement(int nx, int ny)
{
	int px, py; /*piece*/
	int mX, mY; /*map*/
	
	int newx=nx;
	int newy=ny;
	
	 for(mX = sPiece.x, px = 0; mX < (sPiece.x + PIECE_SIZE); mX++, px++)
	  for(mY = sPiece.y, py = 0; mY < (sPiece.y + PIECE_SIZE); mY++, py++)
	   if(sPiece.size[py][px] != 0)
	    {
		 /*The piece is outside the limits of the board*/
		 if((mX < 0) || (mX >= MAP_W) || (mY >= MAP_H))
		  return 0;
				
		 /*The piece have collisioned with a block already stored in the map*/
		 if(playground[mY+newy][mX+newx] != 0)
		  return 0;
		}
	  
	return 1;
}

void Move(int x, int y)
{
	int i, j;
		
    if(checkMovement(x, y))
     {
      sPiece.y+=y;
     }
    else
	 {
	  for(i=0; i<4; i++)
	   for(j=0; j<4; j++)
	    if(sPiece.size[i][j] != 0)
	     playground[sPiece.y+i][sPiece.x+j] = sPiece.size[i][j];
	  
	  PlaySound("pong1.wav");
	  
	  multiLineDelete();
	  if(sPiece.y<=0) //If the tetramino reach top of the board
	   {
	   	PlaySound("pong2.wav");
	    Game.status=2; //is game over!
	   }
	  else
	   assignTetramini(); //Else we create a new tetramino
	 }
	
}

void DrawRect(int x, int y, int width, int height, int color)
 {
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
	    SDL_FillRect(screen, &rect, color);
 }

void DrawText(int x, int y, int width, int height, char *string, void *ptr, Uint8 r, Uint8 g, Uint8 b)
 {
        sprintf(text, string, ptr);
        SDL_Color color = { r, g, b };
        textSurface = TTF_RenderText_Solid(Game.font, text, color);
	    rect.x = x;
		rect.y = y;
	    rect.w = width;
	    rect.h = height;
        SDL_BlitSurface(textSurface, NULL, screen, &rect);
 }

int DrawMenu()
 {
  DrawText(10, 20, 0, 0, "RetroTetris", 0,  0xFF, 0xFF, 0xFF);
  DrawRect(10, 50, SCREEN_WIDTH, 10, YELLOW);
  DrawText(30, 70, 0, 0, "Start", 0, 0xFF, 0xFF, 0xFF);
  DrawText(30, 100, 0, 0, "Level %d", (int*) Game.level, 0xFF, 0xFF, 0xFF);
  DrawText(30, 130, 0, 0, "Quit", 0, 0xFF, 0xFF, 0xFF);
  DrawRect(Ball.x, Ball.y, 10, 10, 0xff0000);
 }

void DrawPlayfield()
 {
  int i, j;
  for (i = 0; i < MAP_H; i++) // This one draws rows at column j
   {
    for (j = 0; j < MAP_W; j++) // This one draws columns at row i
     {
      if(playground[i][j]==8)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, WHITE); //Wall
      if(playground[i][j]==1)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, SKYBLUE);
      if(playground[i][j]==2)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, YELLOW);
      if(playground[i][j]==3)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, PURPLE);
      if(playground[i][j]==4)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, GREEN);
      if(playground[i][j]==5)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, RED);
      if(playground[i][j]==6)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, BLUE);
      if(playground[i][j]==7)
       DrawRect(j * TILESIZE, i * TILESIZE, TILESIZE, TILESIZE, ORANGE);
     }
   }
 }

void DrawBlock() //draw moving tetramini
 {
 	int xmy, ymx;

	for(xmy=0; xmy<4; xmy++)
	 {
	 for(ymx=0; ymx<4; ymx++)
	  {
       if(sPiece.size[xmy][ymx]==1)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, SKYBLUE);
       if(sPiece.size[xmy][ymx]==2)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, YELLOW);
       if(sPiece.size[xmy][ymx]==3)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, PURPLE);
       if(sPiece.size[xmy][ymx]==4)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, GREEN);
       if(sPiece.size[xmy][ymx]==5)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, RED);
       if(sPiece.size[xmy][ymx]==6)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, BLUE);
       if(sPiece.size[xmy][ymx]==7)
        DrawRect((sPiece.x+ymx)*TILESIZE, (sPiece.y+xmy)*TILESIZE, TILESIZE, TILESIZE, ORANGE);
	  }
    }	
 }

void DrawNextBlock()
 {
 	int xmy, ymx;
 	
	//draw next block preview
	for(xmy=0; xmy<4; xmy++)
	 {
	 for(ymx=0; ymx<4; ymx++)
	  {
       if(sPiece.next[xmy][ymx]==1)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, SKYBLUE);
       if(sPiece.next[xmy][ymx]==2)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, YELLOW);
       if(sPiece.next[xmy][ymx]==3)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, PURPLE);
       if(sPiece.next[xmy][ymx]==4)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, GREEN);
       if(sPiece.next[xmy][ymx]==5)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, RED);
       if(sPiece.next[xmy][ymx]==6)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, BLUE);
       if(sPiece.next[xmy][ymx]==7)
        DrawRect(SCREEN_WIDTH-120+(ymx*TILESIZE), 45+(xmy*TILESIZE), TILESIZE, TILESIZE, ORANGE);
	  }
    }
 }

void Draw()
{
  switch(Game.status)
   {
   	case 0:
   	 DrawMenu();
   	 break;
   	case 1:
   	 DrawPlayfield();
   	 DrawBlock();
   	 DrawNextBlock();
   	 DrawText(SCREEN_WIDTH-135, 10, 0, 0, "Next", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 180, 0, 0, "Score", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 150 , 0, 0, "%d", (int *) Game.score, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 120 , 0, 0, "Lines", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 90 , 0, 0, "%d", (int *) Game.lines, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 60 , 0, 0, "Level", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(SCREEN_WIDTH-135, SCREEN_HEIGHT - 30 , 0, 0, "%d", (int *) Game.level, 0xFF, 0xFF, 0xFF);
   	 break;
   	case 2:
   	 DrawText(20, 10, 0, 0, "Game Over", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 50, 0, 0, "Your score is", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 80, 0, 0, "%d", (int *) Game.score, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 110 , 0, 0, "Total lines", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 140, 0, 0, "%d", (int *) Game.lines, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 170, 0, 0, "LEVEL", NULL, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 200, 0, 0, "%d", (int *) Game.level, 0xFF, 0xFF, 0xFF);
   	 DrawText(20, 260, 0, 0, "Press Space", NULL, 0xFF, 0xFF, 0xFF);
     if (SDL_PollEvent(&event))
      if (event.type == SDL_KEYDOWN && event.key.keysym.sym==SDLK_SPACE)
        Game.status=0;
   	 break;
   }
}

void newGame()
 {
	/* Init ingame variables */
    Game.status = 1;
    
  int i, j;
  for (i = 0; i < MAP_H; i++) // This one draws rows at column j
   {
    for (j = 0; j < MAP_W; j++) // This one draws columns at row i
     {
     	if(j==0 || i==MAP_H-1 || j==MAP_W-1) //makes Y-collision easier.
		 {
		 playground[i][j]=8;
         }
		else
		 playground[i][j]=0;
     }
   }
   createTetramini();
   assignTetramini();
   Game.score=0;
   Game.lines=0;
   Game.status=1;
 }

void gameMenu()
 {
 	static int choice, select;
 	
    switch(choice)
	 {
	 	case -1: //Scroll back
	 	 choice=3;
	 	 break;
	 	case 0: //Start game
	 	 Ball.x = 10; Ball.y = 77;
	 	 if (event.key.keysym.sym == SDLK_RETURN)
	 	  newGame();
	 	 break;
	 	case 1: //Level
	 	 Ball.x = 10; Ball.y = 107;
	 	 break;
	 	case 2: //exit
	 	 Ball.x = 10; Ball.y = 137;
	 	 if (event.key.keysym.sym == SDLK_RETURN)
	 	  quit = 1;
	     break;
	 	default:
	 	 choice=0;
	 }
    if (SDL_PollEvent(&event))
     {
      if (event.type == SDL_QUIT)
       quit = 1;
       
      if (event.type == SDL_KEYDOWN)
       {
        keyPressed = event.key.keysym.sym;
        switch (keyPressed)
         {
          case SDLK_ESCAPE:
           quit = 1;
           break;
          case SDLK_UP:
           choice--;
           break;
          case SDLK_DOWN:
           choice++;
           break;
          case SDLK_LEFT:
           if(choice==1 && Game.level>0)
	 	    Game.level--;
           break;
          case SDLK_RIGHT:
           if(choice==1 && Game.level<12)
            Game.level++;
           break;
         }
       }
    }
 }

void logicLoop()
 {
  if(!paused)
   {
    // Handle input
    if (SDL_PollEvent(&event))
     {
      if (event.type == SDL_QUIT)
       quit = 1;

      if (event.type == SDL_KEYDOWN)
       {
       	keyPressed = event.key.keysym.sym;
        switch (keyPressed)
         {
          case SDLK_ESCAPE:
           quit = 1;
           break;
          case SDLK_LEFT:
           if(checkMovement(-1, 0))
            sPiece.x--;
            break;
          case SDLK_RIGHT:
           if(checkMovement(1, 0))
            sPiece.x++;
            break;
          case SDLK_UP:
          case SDLK_z:
          case SDLK_x:
           RotateBlock();
           break;
          case SDLK_DOWN:
           if(checkMovement(0, 1))
            sPiece.y++;
            break;
          case SDLK_c:
          case SDLK_SPACE:
           while(checkMovement(0, 1))
            sPiece.y++;
            break;
         }
        }
      }
	if(t2 +(600-(Game.level*45)) < SDL_GetTicks()) //wait SDL_Getticks rich last_tick value
	 {
	  Move(0, 1);
      t2 = SDL_GetTicks();
	 }
   }
 }

int fps_sync ()
{
	t = SDL_GetTicks ();
	if (t - tl >= frequency)
	{
		temp = (t - tl) / frequency; //delta time
		tl += temp * frequency;
		return temp;
	}
	else
	{
		SDL_Delay (frequency - (t - tl));
		tl += frequency;
		return 1;
	}
}

int mainLoop()
 {
  //main game loop
  while(!quit)
   {
	repeat = fps_sync ();
		for (i = 0; i < repeat; i ++)
		 {
          srand(time(NULL));
   	      switch(Game.status)
   	       {
   	       	case 0: //Menu
   	       	 gameMenu();
   	       	 break;
   	       	case 1: //Game
   	       	 logicLoop();
   	       }
		 }
		//clear the screen
		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
		//draw to the screen
		Draw();
		//update the screen
		SDL_Flip(screen);
   }
 }

int main(int argc, char *argv[])
{
  // init video stuff
	if(SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

   // init screen
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_SWSURFACE);
	if(screen == NULL)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}

    // init ttf and load font
    TTF_Init();
    if ((Game.font = TTF_OpenFont( FONT, FONT_SIZE )) == NULL)
    {
     fprintf( stderr, "Could not open font: %s\n", FONT );
     exit(-1);
    }

	//Initialize SDL_mixer with our chosen audio settings
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}
	
	SDL_WM_SetCaption("RetroTetris", "RetroTetris");
	
	Game.status=0; //Game status 0 means Game Menu
	
	t2 = t;  //last tick since we call this function
	
	//main game loop
	mainLoop();
	printf("Thanks for playing!\n");
	
	SDL_FreeSurface(screen);
	TTF_Quit();
   	SDL_Quit();
	return 0;
}
